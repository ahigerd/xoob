#include "connection.h"
#include "event.h"
#include "window.h"
#include "screen.h"
#include "graphicscontext.h"
#include "font.h"
#include "colormap.h"
#include "pixmap.h"
//#include <xcb/xcb.h>
//#include <xcb/xproto.h>
#include <iostream>
using namespace std;

int main(int, char**) {
    xoob::Connection* conn = xoob::Connection::connect();
    xoob::Screen* s = conn->screen();
    cout << "screen: " << s->width() << "x" << s->height() << " (" << s->widthMM() << "x" << s->heightMM() << ") " << s->depth() << " bit" << endl;

    xoob::Colormap* colors = s->defaultColormap();

    xoob::Window* w = new xoob::Window;
    xoob::GraphicsContext* gc = new xoob::GraphicsContext(w);
    xoob::GraphicsContext* gc2 = new xoob::GraphicsContext(w);
    gc2->setOption(xoob::GraphicsContext::LineWidth, 2);
    gc->setFont(xoob::Font("fixed", conn));
    gc2->setFont(xoob::Font("variable", conn));
    gc2->setOption(xoob::GraphicsContext::Foreground, colors->allocateColor(65535, 0, 0)->pixel());

    xoob::Pixmap* pixmap = new xoob::Pixmap(16, 16, s->depth());
    xoob::GraphicsContext* pixGC = new xoob::GraphicsContext(pixmap);
    pixGC->setOption(xoob::GraphicsContext::Foreground, colors->allocateColor(0, 0, 65535)->pixel());
    pixGC->fillRect(xoob::Rect(0, 0, 16, 16));
    pixGC->setOption(xoob::GraphicsContext::Foreground, colors->allocateColor(0, 65535, 0)->pixel());
    pixGC->setOption(xoob::GraphicsContext::LineWidth, 2);
    pixGC->drawLine(xoob::Line(0, 0, 15, 15));
    pixGC->drawLine(xoob::Line(0, 15, 15, 0));

    xoob::Event* evt = 0;
    xoob::MouseEvent* me = 0;
    w->setWindowName("XOOB Test");
    w->setIconName("XOOB Icon");
    w->show();
    w->move(300,300);
    w->setBorderWidth(5);

    xoob::Polygon poly;
    poly.addPoint(70, 5);
    poly.addPoint(80, 15);
    poly.addPoint(70, 25);
    poly.addPoint(60, 15);
    poly.addPoint(70, 5);
    while(true) {
        gc2->drawLine(xoob::Line(1,1,25,25));
        gc->drawRect(xoob::Rect(30,30,20,20));
        gc->fillRect(xoob::Rect(40,40,20,20));
        gc2->fillArc(xoob::Arc(60,60,30,30,0,90<<6));
        gc->drawArc(xoob::Arc(60,60,30,30,0,360<<6));
        gc2->fillPolygon(poly, xoob::GraphicsContext::Convex);
        gc->fillPolygon(poly, xoob::GraphicsContext::Convex);
        gc->drawText(10, 75, "Hello,");
        gc2->drawText(10, 90, "world!");
        gc->copyPixmap(pixmap, 30, 0);
        gc->copyPixmap(pixmap, 0, 40);
        gc->copyPixmap(pixmap, 65, 30);
        conn->flush();

        if(evt) delete evt;
        if(!(evt = conn->nextEvent(true))) break;
        if((me = dynamic_cast<xoob::MouseEvent*>(evt))) {
            gc->drawPoint(xoob::Point(me->x(), me->y()));
        } else if(evt->responseType() == xoob::Event::KeyPress) {
            xoob::KeyEvent* e = dynamic_cast<xoob::KeyEvent*>(evt);
            cout << "key=" << int(e->keyCode()) << endl;
            conn->beep();
        }
    }
    return 0;
}
