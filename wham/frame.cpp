#include "frame.h"
#include <xoob/connection.h>
#include <xoob/screen.h>
#include <xoob/graphicscontext.h>
#include <xoob/colormap.h>
#include <xoob/font.h>
#include <memory>
#include <iostream>
using namespace std;

Frame::Frame(xoob::Connection* connection) : xoob::Window((xoob::Window*)(0), connection), text(0), light(0), mid(0), dark(0) {
    xoob::Colormap* colors = this->connection()->screen()->defaultColormap();
    std::auto_ptr<xoob::Colormap::Color> white(colors->allocateColor(65535, 65535, 65535)); 
    std::auto_ptr<xoob::Colormap::Color> blue1(colors->allocateColor(0, 0, 16383)); 
    std::auto_ptr<xoob::Colormap::Color> blue2(colors->allocateColor(0, 0, 32767)); 
    std::auto_ptr<xoob::Colormap::Color> blue3(colors->allocateColor(0, 0, 65535)); 
    std::auto_ptr<xoob::Colormap::Color> bg(colors->allocateColor(0, 0, 0)); 

    text = new xoob::GraphicsContext(this);
    text->setFont(xoob::Font("fixed", this->connection()));
    text->setOption(xoob::GraphicsContext::Foreground, white->pixel());

    light = new xoob::GraphicsContext(this);
    light->setOption(xoob::GraphicsContext::Foreground, blue3->pixel());

    mid = new xoob::GraphicsContext(this);
    mid->setOption(xoob::GraphicsContext::Foreground, blue2->pixel());

    dark = new xoob::GraphicsContext(this);
    dark->setOption(xoob::GraphicsContext::Foreground, blue1->pixel());

    black = new xoob::GraphicsContext(this);
    black->setOption(xoob::GraphicsContext::Foreground, bg->pixel());
}

Frame::~Frame() {
    delete text;
    delete light;
    delete mid;
    delete dark;
}

void Frame::paint(xoob::ExposeEvent* event) {
    if(height == -1 || width == -1) {
        xoob::Drawable::Geometry* geom = requestGeometry();
        width = geom->width();
        height = geom->height();
        delete geom;
    }
    black->fillRect(xoob::Rect(0, 0, width, height));
    light->drawRect(xoob::Rect(0, 0, width - 3, height - 3));
    dark->drawRect(xoob::Rect(2, 2, width - 3, height - 3));
    mid->drawRect(xoob::Rect(1, 1, width - 3, height - 3));
    connection()->flush();
    cout << "paint" << endl;
}

void Frame::updateGeometry(xoob::Event* event) {
    cout << event->responseType() << endl;
    if(event->responseType() == xoob::Event::ResizeRequest) {
        width = static_cast<xoob::ResizeRequestEvent*>(event)->newWidth();
        height = static_cast<xoob::ResizeRequestEvent*>(event)->newHeight();
        //width = width - (width % 16);
        //height = height - (height % 16);
        //resize(width, height);
        cout << "resized: " << width << "x" << height << endl;
        paint();
    }
}
