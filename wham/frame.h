#ifndef WHAM_FRAME_H
#define WHAM_FRAME_H

#include <xoob/window.h>
#include <xoob/event.h>
class xoob::GraphicsContext;

class Frame : public xoob::Window {
public:
    Frame(xoob::Connection* connection = 0);
    ~Frame();

    void paint(xoob::ExposeEvent* event = 0);
    void updateGeometry(xoob::Event* event);

private:
    xoob::GraphicsContext* text;
    xoob::GraphicsContext* light;
    xoob::GraphicsContext* mid;
    xoob::GraphicsContext* dark;
    xoob::GraphicsContext* black;
    int height, width;
};

#endif
