#include <xoob/connection.h>
#include <xoob/screen.h>
#include <xoob/window.h>
#include <xoob/colormap.h>
#include "frame.h"
#include <iostream>
using namespace std;

int main(int, char**) {
    xoob::Connection* conn = xoob::Connection::connect();

    Frame f;
    f.show();
    f.paint();
    
    xoob::Event* evt = 0;
    while(true) {
        if(evt) delete evt;
        if(!(evt = conn->nextEvent(true))) break;

        int rtype = evt->responseType();
        cout << "type=" << rtype << endl;
        if(rtype == xoob::Event::Expose) {
            f.paint(static_cast<xoob::ExposeEvent*>(evt));
        } else if(rtype == xoob::Event::ResizeRequest) {
            f.updateGeometry(evt);
        }
    }
}
