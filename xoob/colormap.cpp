#include "colormap.h"
#include "screen.h"
#include "connection.h"
#include "window.h"
#include "cookie_p.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>

namespace xoob {

class Colormap::Private {
public:
    Connection* connection;
    uint32_t handle;
    bool freeOnDestroy;
};

Colormap::Colormap(uint32_t handle, Connection* connection) : d(new Colormap::Private) {
    d->handle = handle;
    d->connection = connection;
    d->freeOnDestroy = false;
}

Colormap::Colormap(AllocationMode mode, Screen* screen) : d(new Colormap::Private) {
    if(!screen) screen = Connection::instance()->screen();
    d->connection = screen->connection();
    d->handle = d->connection->nextID();
    d->freeOnDestroy = true;
    xcb_create_colormap(d->connection->handle(), mode, d->handle, screen->rootWindow()->handle(), screen->handle()->root_visual);
}

Colormap::Colormap(Screen* screen) : d(new Colormap::Private) {
    if(!screen) screen = Connection::instance()->screen();
    d->connection = screen->connection();
    d->handle = d->connection->nextID();
    d->freeOnDestroy = true;
    xcb_create_colormap(d->connection->handle(), XCB_COLORMAP_ALLOC_NONE, d->handle, screen->rootWindow()->handle(), screen->handle()->root_visual);
}

Colormap::~Colormap() {
    if(d->freeOnDestroy)
        xcb_free_colormap(d->connection->handle(), d->handle);
}

uint32_t Colormap::handle() const {
    return d->handle;
}

Connection* Colormap::connection() const {
    return d->connection;
}

Colormap::Color* Colormap::allocateColor(uint16_t red, uint16_t green, uint16_t blue) {
    return new Colormap::Color(this, red, green, blue);
}

XOOB_COOKIE_DEFAULT(Colormap::Color, xcb_alloc_color)

Colormap::Color::Color(Colormap* colormap, uint16_t red, uint16_t green, uint16_t blue) : d(new Colormap::Color::Private) {
    d->connection = colormap->connection();
    d->cookie = xcb_alloc_color(d->connection->handle(), colormap->handle(), red, green, blue);
}

Colormap::Color::~Color() {
    delete d;
}

uint16_t Colormap::Color::red() {
    XOOB_COOKIE_RETURN(red);
}

uint16_t Colormap::Color::green() {
    XOOB_COOKIE_RETURN(green);
}

uint16_t Colormap::Color::blue() {
    XOOB_COOKIE_RETURN(blue);
}

uint32_t Colormap::Color::pixel() {
    XOOB_COOKIE_RETURN(pixel);
}

}
