#ifndef XOOB_COLORMAP_H
#define XOOB_COLORMAP_H

#include <stdint.h>
#include "window.h"

namespace xoob {

class Connection;
class Screen;

class Colormap {
private:
    friend class Screen;
    friend class Window;
    friend class Window::Attributes;
    class Private;
    Private* d;

    Colormap(uint32_t handle, Connection* screen);

public:
    class Color;
    enum AllocationMode { AllocateNone, AllocateAll };

    Colormap(AllocationMode mode, Screen* screen = 0); 
    Colormap(Screen* screen = 0);
    ~Colormap();

    uint32_t handle() const;
    Connection* connection() const;

    Color* allocateColor(uint16_t red, uint16_t green, uint16_t blue);
};

class Colormap::Color {
private:
    friend class Colormap;
    class Private;
    Private* d;

    Color(Colormap* colormap, uint16_t red, uint16_t green, uint16_t blue);

public:
    ~Color();
    
    bool waitForResponse();
    bool error();

    uint16_t red();
    uint16_t green();
    uint16_t blue();
    uint32_t pixel();
};

}

#endif
