#include "connection.h"
#include "event.h"
#include "screen.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>
#include <map>

namespace xoob {

static std::map<std::string, Connection*> xoob_connection_pool;

class Window;
class Connection::Private {
public:
    std::string name;
    xcb_connection_t* connection;
    int defaultScreen;
    const xcb_setup_t* setup;
    int fd;
    std::map<uint32_t, Window*> windows;
};

Connection::Connection(const std::string& name, Connection::Private* pvt) : d(pvt) {
    d->name = name;
    d->setup = xcb_get_setup(d->connection);
    d->fd = xcb_get_file_descriptor(d->connection);
    xoob_connection_pool[name] = this;
}

Connection::~Connection() {
    xoob_connection_pool.erase(d->name);
    if(!xcb_connection_has_error(d->connection))
        xcb_disconnect(d->connection);
    delete d;
}

Connection* Connection::instance(const std::string& name) {
    if(xoob_connection_pool.count(name))
        return xoob_connection_pool[name];
    return NULL;
}

Connection* Connection::connect(const std::string& display, const std::string& name) {
    Private* d = new Private;
    if(display.size() == 0)
        d->connection = xcb_connect(NULL, &(d->defaultScreen));
    else
        d->connection = xcb_connect(display.c_str(), &(d->defaultScreen));
    if(d->connection) 
        return new Connection(name, d);
    delete d;
    return NULL;
}

Connection* Connection::connect(const std::string& display, const AuthInfo& auth, const std::string& name) {
    Private* d = new Private;
    xcb_auth_info_t authInfo;
    authInfo.namelen = auth.name.size();
    authInfo.name = const_cast<char*>(auth.name.c_str());
    authInfo.datalen = auth.data.size();
    authInfo.data = const_cast<char*>(auth.data.c_str());
    d->connection = xcb_connect_to_display_with_auth_info(display.c_str(), &authInfo, &(d->defaultScreen));
    if(d->connection) 
        return new Connection(name, d);
    delete d;
    return NULL;
}

Connection* Connection::connect(int fd, const std::string& name) {
    Private* d = new Private;
    d->connection = xcb_connect_to_fd(fd, NULL);
    if(d->connection) 
        return new Connection(name, d);
    delete d;
    return NULL;
}

Connection* Connection::connect(int fd, const AuthInfo& auth, const std::string& name) {
    Private* d = new Private;
    xcb_auth_info_t authInfo;
    authInfo.namelen = auth.name.size();
    authInfo.name = const_cast<char*>(auth.name.c_str());
    authInfo.datalen = auth.data.size();
    authInfo.data = const_cast<char*>(auth.data.c_str());
    d->connection = xcb_connect_to_fd(fd, &authInfo);
    if(d->connection) 
        return new Connection(name, d);
    delete d;
    return NULL;
}

xcb_connection_t* Connection::handle() const {
    return d->connection;
}

bool Connection::error() {
    return xcb_connection_has_error(d->connection);
}

Event* Connection::nextEvent(bool blocking) {
    xcb_generic_event_t* evt;
    if(blocking)
        evt = xcb_wait_for_event(d->connection);
    else
        evt = xcb_poll_for_event(d->connection);
    if(!evt) return NULL;
    Event::ResponseType t = (Event::ResponseType)evt->response_type;
    switch(t) {
        case Event::KeyPress:
        case Event::KeyRelease:
            return new KeyEvent(evt, this);
        case Event::ButtonPress:
        case Event::ButtonRelease:
        case Event::MotionNotify:
        case Event::EnterNotify:
        case Event::LeaveNotify:
            return new MouseEvent(evt, this);
        /*case Event::FocusIn:           return new FocusInEvent(evt);
        case Event::FocusOut:          return new FocusOutEvent(evt);
        case Event::KeymapNotify:      return new KeymapNotifyEvent(evt);*/
        case Event::Expose:
            return new ExposeEvent(evt, this);
        /*case Event::GraphicsExposure:  return new GraphicsExposureEvent(evt);
        case Event::NoExposure:        return new NoExposureEvent(evt);
        case Event::VisibilityNotify:  return new VisibilityNotifyEvent(evt);
        case Event::CreateNotify:      return new CreateNotifyEvent(evt);
        case Event::DestroyNotify:     return new DestroyNotifyEvent(evt);
        case Event::UnmapNotify:       return new UnmapNotifyEvent(evt);
        case Event::MapNotify:         return new MapNotifyEvent(evt);
        case Event::MapRequest:        return new MapRequestEvent(evt);
        case Event::ReparentNotify:    return new ReparentNotifyEvent(evt);
        case Event::ConfigureNotify:   return new ConfigureNotifyEvent(evt);
        case Event::ConfigureRequest:  return new ConfigureRequestEvent(evt);
        case Event::GravityNotify:     return new GravityNotifyEvent(evt);
        case Event::ResizeRequest:     return new ResizeRequestEvent(evt);
        case Event::CirculateNotify:   return new CirculateNotifyEvent(evt);
        case Event::CirculateRequest:  return new CirculateRequestEvent(evt);
        case Event::PropertyNotify:    return new PropertyNotifyEvent(evt);
        case Event::SelectionClear:    return new SelectionClearEvent(evt);
        case Event::SelectionRequest:  return new SelectionRequestEvent(evt);
        case Event::SelectionNotify:   return new SelectionNotifyEvent(evt);
        case Event::ColormapNotify:    return new ColormapNotifyEvent(evt);
        case Event::ClientMessage:     return new ClientMessageEvent(evt);
        case Event::MappingNotify:     return new MappingNotifyEvent(evt); */
        default: return new Event(evt, this);
    }
}

int Connection::fileDescriptor() const {
    return d->fd;
}

bool Connection::flush() {
    return xcb_flush(d->connection);
}

uint32_t Connection::nextID() {
    return xcb_generate_id(d->connection);
}

int Connection::screenCount() const {
    return xcb_setup_roots_length(d->setup);
}

Screen* Connection::screen(int index) const {
    if(index == -1) index = d->defaultScreen;
    if(index > screenCount()) return NULL;
    xcb_screen_iterator_t iterator = xcb_setup_roots_iterator(d->setup);
    while(index > 0) {
        xcb_screen_next(&iterator);
        index--;
    }
    return new Screen(iterator.data, const_cast<Connection*>(this));
}

void Connection::registerWindow(uint32_t id, Window* ptr) {
    d->windows[id] = ptr;
}

void Connection::unregisterWindow(uint32_t id) {
    d->windows.erase(id);
}

Window* Connection::registeredWindow(uint32_t id) {
    if(d->windows.count(id)) return d->windows[id];
    return NULL;
}

void Connection::beep(int8_t percent) const {
    xcb_bell(d->connection, percent);
}

}
