#ifndef XOOB_CONNECTION_H
#define XOOB_CONNECTION_H

#include <string>
#include <stdint.h>

struct xcb_connection_t;

namespace xoob {

class Event;
class Screen;
class Window;

class AuthInfo {
public:
    std::string name;
    std::string data;
};

class Connection {
private:
    class Private;
    Private* d;

    Connection(const std::string& name, Private* pvt);

public:
    ~Connection();

    static Connection* instance(const std::string& name = std::string());

    static Connection* connect(const std::string& display = std::string(), const std::string& name = std::string());
    static Connection* connect(const std::string& display, const AuthInfo& auth, const std::string& name = std::string());
    static Connection* connect(int fd, const std::string& name = std::string());
    static Connection* connect(int fd, const AuthInfo& auth, const std::string& name = std::string());

    xcb_connection_t* handle() const;

    bool error();
    Event* nextEvent(bool blocking = false);
    int fileDescriptor() const;
    bool flush();

    Screen* screen(int index = -1) const;
    int screenCount() const;

    uint32_t nextID();
    void registerWindow(uint32_t id, Window* ptr);
    void unregisterWindow(uint32_t id);
    Window* registeredWindow(uint32_t id);

    void beep(int8_t percent = 100) const;

    // TODO: add accessors for xcb_setup_t fields if needed
    // TODO: add support for X extensions
};

}

#endif
