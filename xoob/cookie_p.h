#ifndef XOOB_COOKIE_P_H
#define XOOB_COOKIE_P_H

#include <stdint.h>
#include <stdlib.h>
#include <xcb/xcb.h>
#include <xcb/xproto.h>

namespace xoob {

class Connection;

template <typename ReplyType, typename CookieType, ReplyType*(FN)(xcb_connection_t*, CookieType, xcb_generic_error_t**) >
struct Cookie {
    CookieType cookie;
    ReplyType* reply;
    xcb_generic_error_t* error;
    Connection* connection;

    Cookie() : reply(0), error(0) {}

    virtual ~Cookie() {
        if(reply) free(reply);
        if(error) free(error);
    }

    bool waitForResponse() {
        if(error) return false;
        if(reply) return true;
        reply = FN(connection->handle(), cookie, &error);
        return !error;
    }
};

#define XOOB_COOKIE_TYPE(a) xoob::Cookie<a ## _reply_t, a ## _cookie_t, & a ## _reply>

#define XOOB_COOKIE_CLASS(T, a) class T : public XOOB_COOKIE_TYPE(a) {};

#define XOOB_COOKIE_IMPL(T) \
    bool T::waitForResponse() { return d->waitForResponse(); } \
    bool T::error() { return bool(d->error); }

#define XOOB_COOKIE_DEFAULT(T, a) \
    XOOB_COOKIE_CLASS(T::Private, a) \
    XOOB_COOKIE_IMPL(T)

#define XOOB_COOKIE_RETURN_X(m, x) \
    if(!d->waitForResponse()) return x; \
    return d->reply->m;

#define XOOB_COOKIE_RETURN(m) XOOB_COOKIE_RETURN_X(m, 0)

};

#endif

