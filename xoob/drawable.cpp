#include "drawable.h"
#include "connection.h"
#include "window.h"
#include "cookie_p.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>

namespace xoob {

class Drawable::Private {
public:
    Connection* connection;
    uint32_t handle;
};

Drawable::Drawable(uint32_t handle, Connection* connection) : d(new Drawable::Private) {
    if(!connection) connection = Connection::instance();
    d->connection = connection;
    d->handle = handle;
}

Drawable::Drawable(Connection* connection) : d(new Drawable::Private) {
    if(!connection) connection = Connection::instance();
    d->connection = connection;
    d->handle = connection->nextID();
}

Drawable::~Drawable() {
    delete d;
}

uint32_t Drawable::handle() const {
    return d->handle;
}

Connection* Drawable::connection() const {
    return d->connection;
}

Drawable::Geometry* Drawable::requestGeometry() {
    return new Drawable::Geometry(this);
}

XOOB_COOKIE_DEFAULT(Drawable::Geometry, xcb_get_geometry)

Drawable::Geometry::Geometry(Drawable* drawable) : d(new Drawable::Geometry::Private) {
    d->connection = drawable->connection();
    d->cookie = xcb_get_geometry(d->connection->handle(), drawable->handle());
}

Drawable::Geometry::~Geometry() {
    delete d;
}

uint8_t Drawable::Geometry::depth() {
    XOOB_COOKIE_RETURN(depth);
}

Window* Drawable::Geometry::root() {
    if(!waitForResponse()) return 0;
    return d->connection->registeredWindow(d->reply->root);
}

int16_t Drawable::Geometry::x() {
    XOOB_COOKIE_RETURN(x);
}

int16_t Drawable::Geometry::y() {
    XOOB_COOKIE_RETURN(y);
}

uint16_t Drawable::Geometry::width() {
    XOOB_COOKIE_RETURN(width);
}

uint16_t Drawable::Geometry::height() {
    XOOB_COOKIE_RETURN(height);
}

uint16_t Drawable::Geometry::borderWidth() {
    XOOB_COOKIE_RETURN(border_width);
}

}
