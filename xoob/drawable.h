#ifndef XOOB_DRAWABLE_H
#define XOOB_DRAWABLE_H

#include <stdint.h>

namespace xoob {

class Connection;
class Screen;
class GraphicsContext;
class Window;

class Drawable {
private:
    friend class Screen;
    class Private;
    Private* d;

protected:
    Drawable(uint32_t handle, Connection* connection);
    Drawable(Connection* connection = 0);

public:
    class Geometry;

    virtual ~Drawable();

    uint32_t handle() const;
    Connection* connection() const;

    Geometry* requestGeometry();
};

class Drawable::Geometry {
private:
    friend class Drawable;
    class Private;
    Private* d;

    Geometry(Drawable* drawable);

public:
    ~Geometry();
    
    bool waitForResponse();
    bool error();

    uint8_t depth();
    Window* root();
    int16_t x();
    int16_t y();
    uint16_t width();
    uint16_t height();
    uint16_t borderWidth();
};

}

#endif
