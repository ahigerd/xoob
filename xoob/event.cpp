#include "event.h"
#include "connection.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>
#include <malloc.h>

namespace xoob {

Event::Event(void* event, Connection* connection) : genericEvent(event), conn(connection) {
    // initializers only
}

Event::~Event() {
    free(genericEvent);
}

Event::ResponseType Event::responseType() const {
    return Event::ResponseType(reinterpret_cast<xcb_generic_event_t*>(genericEvent)->response_type);
}

uint16_t Event::sequenceNumber() const {
    return reinterpret_cast<xcb_generic_event_t*>(genericEvent)->sequence;
}

Connection* Event::connection() const {
    return conn;
}

InputEvent::InputEvent(void* event, Connection* connection) : Event(event, connection) {
    // initializers only
}

uint32_t InputEvent::timestamp() const {
    return nativeEvent<xcb_key_press_event_t>()->time;
}

uint32_t InputEvent::modifiers() const {
    return nativeEvent<xcb_key_press_event_t>()->state & Event::ModifierMask;
}

int16_t InputEvent::globalX() const {
    return nativeEvent<xcb_key_press_event_t>()->root_x;
}

int16_t InputEvent::globalY() const {
    return nativeEvent<xcb_key_press_event_t>()->root_y;
}

int16_t InputEvent::x() const {
    return nativeEvent<xcb_key_press_event_t>()->event_x;
}

int16_t InputEvent::y() const {
    return nativeEvent<xcb_key_press_event_t>()->event_y;
}

Window* InputEvent::root() const {
    // TODO: what does this mean?
    return connection()->registeredWindow(nativeEvent<xcb_key_press_event_t>()->root);
}

Window* InputEvent::target() const {
    // TODO: what does this mean?
    return connection()->registeredWindow(nativeEvent<xcb_key_press_event_t>()->event);
}

Window* InputEvent::child() const {
    // TODO: what does this mean?
    return connection()->registeredWindow(nativeEvent<xcb_key_press_event_t>()->child);
}

KeyEvent::KeyEvent(void* event, Connection* connection) : InputEvent(event, connection) {
    // initializers only
}

uint8_t KeyEvent::keyCode() const {
    return nativeEvent<xcb_key_press_event_t>()->detail;
}

MouseEvent::MouseEvent(void* event, Connection* connection) : InputEvent(event, connection) {
    // initializers only
}

uint32_t MouseEvent::buttons() const {
    return nativeEvent<xcb_button_press_event_t>()->state & Event::ButtonMask;
}

ExposeEvent::ExposeEvent(void* event, Connection* connection) : Event(event, connection) {
    // initializers only
}

Window* ExposeEvent::target() const {
    return connection()->registeredWindow(nativeEvent<xcb_expose_event_t>()->window);
}
uint16_t ExposeEvent::x() const {
    return nativeEvent<xcb_expose_event_t>()->x;
}

uint16_t ExposeEvent::y() const {
    return nativeEvent<xcb_expose_event_t>()->y;
}

uint16_t ExposeEvent::width() const {
    return nativeEvent<xcb_expose_event_t>()->width;
}

uint16_t ExposeEvent::height() const {
    return nativeEvent<xcb_expose_event_t>()->height;
}

ResizeRequestEvent::ResizeRequestEvent(void* event, Connection* connection) : Event(event, connection) {
    // initializers only
}

Window* ResizeRequestEvent::target() const {
    return connection()->registeredWindow(nativeEvent<xcb_resize_request_event_t>()->window);
}

uint16_t ResizeRequestEvent::newWidth() const {
    return nativeEvent<xcb_resize_request_event_t>()->width;
}

uint16_t ResizeRequestEvent::newHeight() const {
    return nativeEvent<xcb_resize_request_event_t>()->height;
}

}
