#ifndef XOOB_EVENT_H
#define XOOB_EVENT_H

#include <stdint.h>

namespace xoob {

class Connection;
class Window;

class Event {
public:
    enum ResponseType {
        Invalid = 0,
        KeyPress = 2,
        KeyRelease,
        ButtonPress,
        ButtonRelease,
        MotionNotify,
        EnterNotify,
        LeaveNotify,
        FocusIn,
        FocusOut,
        KeymapNotify,
        Expose,
        GraphicsExposure,
        NoExposure,
        VisibilityNotify,
        CreateNotify,
        DestroyNotify,
        UnmapNotify,
        MapNotify,
        MapRequest,
        ReparentNotify,
        ConfigureNotify,
        ConfigureRequest,
        GravityNotify,
        ResizeRequest,
        CirculateNotify,
        CirculateRequest,
        PropertyNotify,
        SelectionClear,
        SelectionRequest,
        SelectionNotify,
        ColormapNotify,
        ClientMessage,
        MappingNotify
    };

    enum EventMask {
        NoEventMask              = 0x00000000,
        KeyPressMask             = 1 << 0,
        KeyReleaseMask           = 1 << 1,
        ButtonPressMask          = 1 << 2,
        ButtonReleaseMask        = 1 << 3,
        EnterNotifyMask          = 1 << 4,
        LeaveNotifyMask          = 1 << 5,
        PointerMotionMask        = 1 << 6,
        PointerMotionHintMask    = 1 << 7,
        Button1MotionMask        = 1 << 8,
        Button2MotionMask        = 1 << 9,
        Button3MotionMask        = 1 << 10,
        Button4MotionMask        = 1 << 11,
        Button5MotionMask        = 1 << 12,
        ButtonMotionMask         = 1 << 13,
        KeymapStateMask          = 1 << 14,
        ExposeMask               = 1 << 15,
        VisibilityChangeMask     = 1 << 16,
        StructureNotifyMask      = 1 << 17,
        ResizeRedirectMask       = 1 << 18,
        SubstructureNotifyMask   = 1 << 19,
        SubstructureRedirectMask = 1 << 20,
        FocusChangeMask          = 1 << 21,
        PropertyChangeMask       = 1 << 22,
        ColormapChangeMask       = 1 << 23,
        GrabButtonMask           = 1 << 24,
        AllEventsMask            = 0xFFFFFF 
    };

    enum KeyboardModifiers {
        ShiftModifier   = 1 << 0,
        LockModifier    = 1 << 1,
        ControlModifier = 1 << 2,
        Mod1Modifier    = 1 << 3,
        Mod2Modifier    = 1 << 4,
        Mod3Modifier    = 1 << 5,
        Mod4Modifier    = 1 << 6,
        Mod5Modifier    = 1 << 7,
        ModifierMask    = 0xFF
    };

    enum MouseButtons {
        Button1    = 1 << 8,
        Button2    = 1 << 9,
        Button3    = 1 << 10,
        Button4    = 1 << 11,
        Button5    = 1 << 12,
        AnyButton  = 1 << 15,
        ButtonMask = 0x9F00
    };

    Event(void* event, Connection* connection);
    virtual ~Event();

    ResponseType responseType() const;
    uint16_t sequenceNumber() const;
    Connection* connection() const;

protected:
    template<typename T> inline T* nativeEvent() const {
        return reinterpret_cast<T*>(genericEvent);
    }

private:
    void* genericEvent;
    Connection* conn;
};

class InputEvent : public Event {
public:
    InputEvent(void* event, Connection* connection);

    uint32_t timestamp() const;

    uint32_t modifiers() const;

    int16_t globalX() const;
    int16_t globalY() const;
    int16_t x() const;
    int16_t y() const;

    Window* root() const;
    Window* target() const;
    Window* child() const;
};

class KeyEvent : public InputEvent {
public:
    KeyEvent(void* event, Connection* connection);

    uint8_t keyCode() const;

};

class MouseEvent : public InputEvent {
public:
    MouseEvent(void* event, Connection* connection);

    uint32_t buttons() const;
};

class ExposeEvent : public Event {
public:
    ExposeEvent(void* event, Connection* connection);

    Window* target() const;

    uint16_t x() const;
    uint16_t y() const;
    uint16_t width() const;
    uint16_t height() const;
};

class ResizeRequestEvent : public Event {
public:
    ResizeRequestEvent(void* event, Connection* connection);

    Window* target() const;

    uint16_t newWidth() const;
    uint16_t newHeight() const;
};

class ConfigureNotifyEvent : public Event {
public:
    ResizeRequestEvent(void* event, Connection* connection);

    Window* target() const;
    Window* window() const;
    Window* above_sibling() const;

    int16_t newX() const;
    int16_t newY() const;
    uint16_t newWidth() const;
    uint16_t newHeight() const;
    uint16_t newBorderWidth() const;
    uint8_t overrideRedirect() const;
};

}
#endif
