#include "font.h"
#include "connection.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>

namespace xoob {

class Font::Private {
public:
    Connection* connection;
    uint32_t handle;
};

Font::Font(const std::string& name, Connection* connection) : d(new Font::Private) {
    if(!connection) connection = Connection::instance();
    d->connection = connection;
    d->handle = connection->nextID();
    xcb_open_font(connection->handle(), d->handle, name.size(), name.c_str());
}

Font::~Font() {
    xcb_close_font(d->connection->handle(), d->handle);
    delete d;
}

Connection* Font::connection() const {
    return d->connection;
}

uint32_t Font::handle() const {
    return d->handle;
}

}
