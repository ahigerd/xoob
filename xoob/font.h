#ifndef XOOB_FONT_H
#define XOOB_FONT_H

#include <string>

namespace xoob {

class Connection;

class Font {
private:
    class Private;
    Private* d;

public:
    Font(const std::string& name, Connection* connection = 0);
    ~Font();

    Connection* connection() const;
    uint32_t handle() const;
};

}

#endif
