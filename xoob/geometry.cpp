#include "geometry.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>
#include <vector>

#define D_RC(t) reinterpret_cast<xcb_ ## t ## _t*>(d)

namespace xoob {

Point::Point(const Point& other) : d(new xcb_point_t(*reinterpret_cast<xcb_point_t*>(other.d))) {
    // initializers only
}

Point::Point(int16_t x, int16_t y) : d(new xcb_point_t) {
    D_RC(point)->x = x;
    D_RC(point)->y = y;
}

Point::~Point() {
    delete D_RC(point);
}

int16_t Point::x() const {
    return D_RC(point)->x;
}

int16_t Point::y() const {
    return D_RC(point)->y;
}

void Point::setX(int16_t x) {
    D_RC(point)->x = x;
}

void Point::setY(int16_t y) {
    D_RC(point)->y = y;
}

Line::Line(const Line& other) : d(new xcb_segment_t(*reinterpret_cast<xcb_segment_t*>(other.d))) {
    // initializers only
}

Line::Line(const Point& p1, const Point& p2) : d(new xcb_segment_t) {
    D_RC(segment)->x1 = p1.x();
    D_RC(segment)->y1 = p1.y();
    D_RC(segment)->x2 = p2.x();
    D_RC(segment)->y2 = p2.y();
}

Line::Line(int16_t x1, int16_t y1, int16_t x2, int16_t y2) : d(new xcb_segment_t) {
    D_RC(segment)->x1 = x1;
    D_RC(segment)->y1 = y1;
    D_RC(segment)->x2 = x2;
    D_RC(segment)->y2 = y2;
}

Line::~Line() {
    delete D_RC(segment);
}

int16_t Line::x1() const {
    return D_RC(segment)->x1;
}

int16_t Line::y1() const {
    return D_RC(segment)->y1;
}

int16_t Line::x2() const {
    return D_RC(segment)->x2;
}

int16_t Line::y2() const {
    return D_RC(segment)->y2;
}

void Line::setX1(int16_t x) {
    D_RC(segment)->x1 = x;
}

void Line::setY1(int16_t y) {
    D_RC(segment)->y1 = y;
}

void Line::setX2(int16_t x) {
    D_RC(segment)->x2 = x;
}

void Line::setY2(int16_t y) {
    D_RC(segment)->y2 = y;
}

Rect::Rect(const Rect& other) : d(new xcb_rectangle_t(*reinterpret_cast<xcb_rectangle_t*>(other.d))) {
    // initializers only
}

Rect::Rect(const Point& p1, const Point& p2) : d(new xcb_rectangle_t) {
    D_RC(rectangle)->x = p1.x();
    D_RC(rectangle)->y = p1.y();
    D_RC(rectangle)->width = p2.x() - p1.x();
    D_RC(rectangle)->height = p2.y() - p1.y();
}

Rect::Rect(int16_t x, int16_t y, uint16_t w, uint16_t h) : d(new xcb_rectangle_t) {
    D_RC(rectangle)->x = x;
    D_RC(rectangle)->y = y;
    D_RC(rectangle)->width = w;
    D_RC(rectangle)->height = h;
}

Rect::~Rect() {
    delete D_RC(rectangle);
}

int16_t Rect::x() const {
    return D_RC(rectangle)->x;
}

int16_t Rect::y() const {
    return D_RC(rectangle)->y;
}

uint16_t Rect::width() const {
    return D_RC(rectangle)->width;
}

uint16_t Rect::height() const {
    return D_RC(rectangle)->height;
}

void Rect::setX(int16_t x) {
    D_RC(rectangle)->x = x;
}

void Rect::setY(int16_t y) {
    D_RC(rectangle)->y = y;
}

void Rect::setWidth(uint16_t w) {
    D_RC(rectangle)->width = w;
}

void Rect::setHeight(uint16_t h) {
    D_RC(rectangle)->height = h;
}

class Polygon::Private : public std::vector<xcb_point_t> {
public:
    xcb_point_t* points() {
        return &front();
    }

    uint8_t mode;
};

Polygon::Polygon() : d(new Polygon::Private) {
    d->mode = XCB_COORD_MODE_ORIGIN;
}

Polygon::Polygon(const Polygon& other) : d(new Polygon::Private(*other.d)) {
    // initializers only
}

Polygon::~Polygon() {
    delete d;
}

Polygon::CoordinateMode Polygon::coordinateMode() const {
    if(d->mode == XCB_COORD_MODE_ORIGIN) return Origin;
    return Previous;
}

void Polygon::setCoordinateMode(Polygon::CoordinateMode mode) {
    if(mode == Origin)
        d->mode = XCB_COORD_MODE_ORIGIN;
    else
        d->mode = XCB_COORD_MODE_PREVIOUS;
}

int Polygon::count() const {
    return d->size();
}

Point Polygon::point(int index) const {
    xcb_point_t p = (*d)[index];
    return Point(p.x, p.y);
}

void Polygon::setPoint(int index, const Point& p) {
    (*d)[index] = *reinterpret_cast<xcb_point_t*>(p.d);
}

void Polygon::addPoint(const Point& p) {
    d->push_back(*reinterpret_cast<xcb_point_t*>(p.d));
}

void* Polygon::data() const {
    return d->points();
}

Arc::Arc(const Arc& other) : d(new xcb_arc_t(*reinterpret_cast<xcb_arc_t*>(other.d))) {
    // initializers only
}

Arc::Arc(int16_t x, int16_t y, uint16_t w, uint16_t h, int16_t s, int16_t e) : d(new xcb_arc_t) {
    D_RC(arc)->x = x;
    D_RC(arc)->y = y;
    D_RC(arc)->width = w;
    D_RC(arc)->height = h;
    D_RC(arc)->angle1 = s;
    D_RC(arc)->angle2 = e;
}

Arc::~Arc() {
    delete D_RC(arc);
}

int16_t Arc::x() const {
    return D_RC(arc)->x;
}

int16_t Arc::y() const {
    return D_RC(arc)->y;
}

uint16_t Arc::width() const {
    return D_RC(arc)->width;
}

uint16_t Arc::height() const {
    return D_RC(arc)->height;
}

int16_t Arc::start() const {
    return D_RC(arc)->angle1;
}

int16_t Arc::end() const {
    return D_RC(arc)->angle2;
}

void Arc::setX(int16_t x) {
    D_RC(arc)->x = x;
}

void Arc::setY(int16_t y) {
    D_RC(arc)->y = y;
}

void Arc::setWidth(uint16_t w) {
    D_RC(arc)->width = w;
}

void Arc::setHeight(uint16_t h) {
    D_RC(arc)->height = h;
}

void Arc::setStart(int16_t s) {
    D_RC(arc)->angle1 = s;
}

void Arc::setEnd(int16_t e) {
    D_RC(arc)->angle2 = e;
}

}
