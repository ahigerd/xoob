#ifndef XOOB_GEOMETRY_H
#define XOOB_GEOMETRY_H

#include <stdint.h>

namespace xoob {

class GraphicsContext;

class Point {
private:
    friend class GraphicsContext;
    friend class Polygon;
    void* d;

public:
    Point(const Point& other);
    Point(int16_t x, int16_t y);
    ~Point();
    
    int16_t x() const;
    int16_t y() const;

    void setX(int16_t x);
    void setY(int16_t y);
};

class Line {
private:
    friend class GraphicsContext;
    void* d;

public:
    Line(const Line& other);
    Line(const Point& p1, const Point& p2);
    Line(int16_t x1, int16_t y1, int16_t x2, int16_t y2);
    ~Line();

    int16_t x1() const;
    int16_t y1() const;
    int16_t x2() const;
    int16_t y2() const;

    void setX1(int16_t x);
    void setY1(int16_t y);
    void setX2(int16_t x);
    void setY2(int16_t y);
};

class Rect {
private:
    friend class GraphicsContext;
    void* d;

public:
    Rect(const Rect& other);
    Rect(const Point& p1, const Point& p2);
    Rect(int16_t x, int16_t y, uint16_t w, uint16_t h);
    ~Rect();

    int16_t x() const;
    int16_t y() const;
    uint16_t width() const;
    uint16_t height() const;

    void setX(int16_t x);
    void setY(int16_t y);
    void setWidth(uint16_t w);
    void setHeight(uint16_t h);
};

class Polygon {
private:
    friend class GraphicsContext;
    class Private;
    Private* d;

public:
    enum CoordinateMode { Origin, Previous };

    Polygon();
    Polygon(const Polygon& other);
    ~Polygon();

    CoordinateMode coordinateMode() const;
    void setCoordinateMode(CoordinateMode mode);

    int count() const;
    Point point(int index) const;

    void setPoint(int index, const Point& p);
    inline void setPoint(int index, int16_t x, int16_t y) { setPoint(index, Point(x, y)); }
    void addPoint(const Point& p);
    inline void addPoint(int16_t x, int16_t y) { addPoint(Point(x, y)); }

    void* data() const;
};

class Arc {
private:
    friend class GraphicsContext;
    void* d;

public:
    Arc(const Arc& other);
    Arc(int16_t x, int16_t y, uint16_t w, uint16_t h, int16_t s, int16_t e);
    ~Arc();

    int16_t x() const;
    int16_t y() const;
    uint16_t width() const;
    uint16_t height() const;
    int16_t start() const;
    int16_t end() const;

    void setX(int16_t x);
    void setY(int16_t y);
    void setWidth(uint16_t w);
    void setHeight(uint16_t h);
    void setStart(int16_t s);
    void setEnd(int16_t e);
};

}

#endif
