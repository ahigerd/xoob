#include "graphicscontext.h"
#include "connection.h"
#include "drawable.h"
#include "screen.h"
#include "font.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>

namespace xoob {

class GraphicsContext::Private {
public:
    Drawable* drawable;
    Connection* connection;
    uint32_t handle;
};

GraphicsContext::GraphicsContext(Drawable* drawable) : d(new GraphicsContext::Private) {
    d->drawable = drawable;
    d->connection = drawable->connection();
    d->handle = d->connection->nextID();
    uint32_t mask = XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES;
    uint32_t values[] = { d->connection->screen()->handle()->white_pixel, 0 };
    xcb_create_gc(d->connection->handle(), d->handle, drawable->handle(), mask, values);
}

GraphicsContext::~GraphicsContext() {
    xcb_free_gc(d->connection->handle(), d->handle);
    delete d;
}

uint32_t GraphicsContext::handle() const {
    return d->handle;
}

Drawable* GraphicsContext::drawable() const {
    return d->drawable;
}

void GraphicsContext::setOption(ContextOption option, uint32_t value) {
    xcb_change_gc(d->connection->handle(), d->handle, option, &value);
}

void GraphicsContext::setOptions(uint32_t options, const uint32_t values[]) {
    xcb_change_gc(d->connection->handle(), d->handle, options, values);
}

void GraphicsContext::setFont(const Font& font) {
    uint32_t fontHandle = font.handle();
    xcb_change_gc(d->connection->handle(), d->handle, SelectedFont, &fontHandle);
}

void GraphicsContext::drawPoint(const Point& p) {
    xcb_poly_point(d->connection->handle(), XCB_COORD_MODE_ORIGIN, d->drawable->handle(), d->handle, 1, reinterpret_cast<xcb_point_t*>(p.d));
}

void GraphicsContext::drawPoints(const Polygon& p) {
    uint8_t mode = XCB_COORD_MODE_ORIGIN;
    if(p.coordinateMode() == Polygon::Previous) mode = XCB_COORD_MODE_PREVIOUS;
    xcb_poly_point(d->connection->handle(), mode, d->drawable->handle(), d->handle, p.count(), reinterpret_cast<xcb_point_t*>(p.data()));
}

void GraphicsContext::drawLine(const Line& p) {
    xcb_poly_segment(d->connection->handle(), d->drawable->handle(), d->handle, 1, reinterpret_cast<xcb_segment_t*>(p.d));
}

void GraphicsContext::drawLines(const std::vector<Line>& p) {
    int size = p.size();
    std::vector<xcb_segment_t> data(size);
    for(int i = size - 1; i > 0; --i) data[i] = *reinterpret_cast<xcb_segment_t*>(p[i].d);
    xcb_poly_segment(d->connection->handle(), d->drawable->handle(), d->handle, size, &data.front());
}

void GraphicsContext::drawRect(const Rect& p) {
    xcb_poly_rectangle(d->connection->handle(), d->drawable->handle(), d->handle, 1, reinterpret_cast<xcb_rectangle_t*>(p.d));
}

void GraphicsContext::drawRects(const std::vector<Rect>& p) {
    int size = p.size();
    std::vector<xcb_rectangle_t> data(size);
    for(int i = size - 1; i > 0; --i) data[i] = *reinterpret_cast<xcb_rectangle_t*>(p[i].d);
    xcb_poly_rectangle(d->connection->handle(), d->drawable->handle(), d->handle, size, &data.front());
}

void GraphicsContext::fillRect(const Rect& p) {
    xcb_poly_fill_rectangle(d->connection->handle(), d->drawable->handle(), d->handle, 1, reinterpret_cast<xcb_rectangle_t*>(p.d));
}

void GraphicsContext::fillRects(const std::vector<Rect>& p) {
    int size = p.size();
    std::vector<xcb_rectangle_t> data(size);
    for(int i = size - 1; i > 0; --i) data[i] = *reinterpret_cast<xcb_rectangle_t*>(p[i].d);
    xcb_poly_fill_rectangle(d->connection->handle(), d->drawable->handle(), d->handle, size, &data.front());
}

void GraphicsContext::drawPolygon(const Polygon& p) {
    uint8_t mode = XCB_COORD_MODE_ORIGIN;
    if(p.coordinateMode() == Polygon::Previous) mode = XCB_COORD_MODE_PREVIOUS;
    xcb_poly_line(d->connection->handle(), mode, d->drawable->handle(), d->handle, p.count(), reinterpret_cast<xcb_point_t*>(p.data()));
}

void GraphicsContext::fillPolygon(const Polygon& p, PolygonType t) {
    uint8_t mode = XCB_COORD_MODE_ORIGIN;
    if(p.coordinateMode() == Polygon::Previous) mode = XCB_COORD_MODE_PREVIOUS;
    uint8_t shape = XCB_POLY_SHAPE_COMPLEX;
    if(t == Nonconvex) shape = XCB_POLY_SHAPE_NONCONVEX;
    else if(t == Convex) shape = XCB_POLY_SHAPE_CONVEX;
    xcb_fill_poly(d->connection->handle(), d->drawable->handle(), d->handle, shape, mode, p.count(), reinterpret_cast<xcb_point_t*>(p.data()));
}

void GraphicsContext::drawArc(const Arc& p) {
    xcb_poly_arc(d->connection->handle(), d->drawable->handle(), d->handle, 1, reinterpret_cast<xcb_arc_t*>(p.d));
}

void GraphicsContext::drawArcs(const std::vector<Arc>& p) {
    int size = p.size();
    std::vector<xcb_arc_t> data(size);
    for(int i = size - 1; i > 0; --i) data[i] = *reinterpret_cast<xcb_arc_t*>(p[i].d);
    xcb_poly_arc(d->connection->handle(), d->drawable->handle(), d->handle, size, &data.front());
}

void GraphicsContext::fillArc(const Arc& p) {
    xcb_poly_fill_arc(d->connection->handle(), d->drawable->handle(), d->handle, 1, reinterpret_cast<xcb_arc_t*>(p.d));
}

void GraphicsContext::fillArcs(const std::vector<Arc>& p) {
    int size = p.size();
    std::vector<xcb_arc_t> data(size);
    for(int i = size - 1; i > 0; --i) data[i] = *reinterpret_cast<xcb_arc_t*>(p[i].d);
    xcb_poly_fill_arc(d->connection->handle(), d->drawable->handle(), d->handle, size, &data.front());
}

void GraphicsContext::drawText(int16_t x, int16_t y, const std::string& text) {
    xcb_image_text_8(d->connection->handle(), text.size(), d->drawable->handle(), d->handle, x, y, text.c_str());
}

void GraphicsContext::drawText(int16_t x, int16_t y, const std::wstring& text) {
    xcb_image_text_16(d->connection->handle(), text.size(), d->drawable->handle(), d->handle, x, y, reinterpret_cast<const xcb_char2b_t*>(text.c_str()));
}

/* GraphicsContext::copyPixmap implemented in pixmap.cpp */

}
