#ifndef XOOB_GRAPHICSCONTEXT_H
#define XOOB_GRAPHICSCONTEXT_H

#include <stdint.h>
#include <vector>
#include <string>
#include "geometry.h"

namespace xoob {

class Connection;
class Drawable;
class Font;
class Pixmap;

class GraphicsContext {
private:
    friend class Drawable;
    class Private;
    Private* d;

public:
    enum PolygonType { Complex, Nonconvex, Convex };
    enum ContextOption {
        Function          = 1 << 0,
        PlaneMask         = 1 << 1,
        Foreground        = 1 << 2,
        Background        = 1 << 3,
        LineWidth         = 1 << 4,
        LineStyle         = 1 << 5,
        CapStyle          = 1 << 6,
        JoinStyle         = 1 << 7,
        FillStyle         = 1 << 8,
        FillRule          = 1 << 9,
        Tile              = 1 << 10,
        Stipple           = 1 << 11,
        TileStippleX      = 1 << 12,
        TileStippleY      = 1 << 13,
        SelectedFont      = 1 << 14,
        SubwindowMode     = 1 << 15,
        GraphicsExposures = 1 << 16,
        ClipX             = 1 << 17,
        ClipY             = 1 << 18,
        ClipMask          = 1 << 19,
        DashOffset        = 1 << 20,
        DashList          = 1 << 21,
        ArcMode           = 1 << 22
    };

    GraphicsContext(Drawable* drawable);
    ~GraphicsContext();

    uint32_t handle() const;
    Drawable* drawable() const;

    void setOption(ContextOption option, uint32_t value);
    void setOptions(uint32_t options, const uint32_t values[]);
    void setFont(const Font& font);

    void drawPoint(const Point& p);
    void drawPoints(const Polygon& p); 

    void drawLine(const Line& p);
    void drawLines(const std::vector<Line>& p); 
    
    void drawRect(const Rect& p);
    void drawRects(const std::vector<Rect>& p);
    void fillRect(const Rect& p);
    void fillRects(const std::vector<Rect>& p);
    
    void drawPolygon(const Polygon& p);
    void fillPolygon(const Polygon& p, PolygonType t = Complex);

    void drawArc(const Arc& p);
    void drawArcs(const std::vector<Arc>& p);
    void fillArc(const Arc& p);
    void fillArcs(const std::vector<Arc>& p);

    void drawText(int16_t x, int16_t y, const std::string& text);
    void drawText(int16_t x, int16_t y, const std::wstring& text);

    void copyPixmap(Pixmap* pixmap, int16_t x = 0, int16_t y = 0);
    void copyPixmap(Pixmap* pixmap, int16_t top, int16_t left, uint16_t width, uint16_t height, int16_t x = 0, int16_t y = 0);
};

}

#endif
