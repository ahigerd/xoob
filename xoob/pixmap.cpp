#include "pixmap.h"
#include "connection.h"
#include "screen.h"
#include "window.h"
#include "graphicscontext.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>

namespace xoob {

struct Pixmap::Private {
    uint16_t width;
    uint16_t height;
    void init(Pixmap* pixmap, uint16_t width, uint16_t height, uint16_t depth, uint32_t drawable) {
        this->width = width;
        this->height = height;
        xcb_create_pixmap(pixmap->connection()->handle(), depth, pixmap->handle(), drawable, width, height); 
    }
};

Pixmap::Pixmap(uint16_t width, uint16_t height, uint8_t depth, Connection* connection) : Drawable(connection), d(new Pixmap::Private) {
    d->init(this, width, height, depth, this->connection()->screen()->rootWindow()->handle());
}

Pixmap::Pixmap(uint16_t width, uint16_t height, uint8_t depth, Screen* screen) : Drawable(screen->connection()), d(new Pixmap::Private) {
    d->init(this, width, height, depth, screen->rootWindow()->handle());
}

Pixmap::Pixmap(uint16_t width, uint16_t height, uint8_t depth, Drawable* screen) : Drawable(screen->connection()), d(new Pixmap::Private) {
    d->init(this, width, height, depth, screen->handle());
}

Pixmap::~Pixmap() {
    xcb_free_pixmap(connection()->handle(), handle());
}

void GraphicsContext::copyPixmap(Pixmap* pixmap, int16_t x, int16_t y) {
    copyPixmap(pixmap, 0, 0, pixmap->d->width, pixmap->d->height, x, y);
}

void GraphicsContext::copyPixmap(Pixmap* pixmap, int16_t top, int16_t left, uint16_t width, uint16_t height, int16_t x, int16_t y) {
    xcb_copy_area(pixmap->connection()->handle(), pixmap->handle(), drawable()->handle(), handle(), top, left, x, y, width, height); 
}

}
