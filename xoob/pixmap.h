#ifndef XOOB_PIXMAP_H
#define XOOB_PIXMAP_H

#include "drawable.h"

namespace xoob {

class Screen;

class Pixmap : public Drawable {
private:
    friend class GraphicsContext;
    class Private;
    Private* d;

public:
    Pixmap(uint16_t width, uint16_t height, uint8_t depth, Connection* connection = 0);
    Pixmap(uint16_t width, uint16_t height, uint8_t depth, Screen* screen);
    Pixmap(uint16_t width, uint16_t height, uint8_t depth, Drawable* screen);
    ~Pixmap();
};

}

#endif
