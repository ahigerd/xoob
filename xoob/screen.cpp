#include "screen.h"
#include "connection.h"
#include "window.h"
#include "colormap.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>

namespace xoob {

class Screen::Private {
public:
    xcb_screen_t* handle;
    Connection* connection;
};

Screen::Screen(xcb_screen_t* screen, Connection* connection) : d(new Screen::Private) {
    d->handle = screen;
    d->connection = connection;
}

Screen::~Screen() {
    delete d;
}

xcb_screen_t* Screen::handle() const {
    return d->handle;
}

int Screen::widthMM() const {
    return d->handle->width_in_millimeters;
}

int Screen::heightMM() const {
    return d->handle->height_in_millimeters;
}

int Screen::width() const {
    return d->handle->width_in_pixels;
}

int Screen::height() const {
    return d->handle->height_in_pixels;
}

int Screen::depth() const {
    return d->handle->root_depth;
}

Connection* Screen::connection() const {
    return d->connection;
}

Window* Screen::rootWindow() const {
    Window* w = d->connection->registeredWindow(d->handle->root);
    if(w) return w;
    return new Window(d->handle->root, d->connection);
}

Colormap* Screen::defaultColormap() {
    return new Colormap(d->handle->default_colormap, d->connection);
}

}
