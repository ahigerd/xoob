#ifndef XOOB_SCREEN_H
#define XOOB_SCREEN_H

struct xcb_screen_t;

namespace xoob {

class Connection;
class Window;
class Colormap;

class Screen {
private:
    friend class Connection;
    class Private;
    Private* d;

    Screen(xcb_screen_t* screen, Connection* connection);
    ~Screen();

public:
    xcb_screen_t* handle() const;

    int widthMM() const;
    int heightMM() const;
    int width() const;
    int height() const;
    int depth() const;

    Connection* connection() const;
    Window* rootWindow() const;
    Colormap* defaultColormap();
};

}

#endif
