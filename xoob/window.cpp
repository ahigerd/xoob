#include "window.h"
#include "connection.h"
#include "screen.h"
#include "colormap.h"
#include "atoms.h"
#include "cookie_p.h"
#include <xcb/xcb.h>
#include <xcb/xproto.h>

namespace xoob {

class Window::Private {
public:
    void init(xcb_window_t handle, WindowClass windowClass, uint8_t depth, Window* parent, Connection* connection) {
        xcb_window_t parentHandle;
        Screen* screen = connection->screen();
        if(parent) {
            parentHandle = parent->handle();
        } else {
            parentHandle = screen->rootWindow()->handle();
        }

        uint32_t values[] = { screen->handle()->black_pixel, Event::AllEventsMask & ~Event::ResizeRedirectMask };
        xcb_create_window(connection->handle(), depth, handle, parentHandle, 0, 0, 150, 150, 10,
                          (uint16_t)windowClass, screen->handle()->root_visual, XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK, values);
    }
};

Window::Window(uint32_t handle, Connection* connection) : Drawable(handle, connection), d(new Window::Private) {
    // initializers only
    this->connection()->registerWindow(handle, this);
}

Window::Window(Window* parent, Connection* connection) : Drawable(connection), d(new Window::Private) {
    d->init(handle(), InputOutput, 0, parent, this->connection());
    this->connection()->registerWindow(handle(), this);
}

Window::Window(WindowClass windowClass, uint8_t depth, Window* parent, Connection* connection) : Drawable(connection), d(new Window::Private) {
    d->init(handle(), windowClass, depth, parent, this->connection());
    this->connection()->registerWindow(handle(), this);
}

Window::~Window() {
    connection()->unregisterWindow(handle());
    delete d;
}

void Window::show() {
    xcb_map_window(connection()->handle(), handle());
    connection()->flush();
}

void Window::hide() {
    xcb_unmap_window(connection()->handle(), handle());
    connection()->flush();
}

void Window::setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::string& data) {
    xcb_change_property(connection()->handle(), mode, handle(), propertyAtom, typeAtom, 8, data.length(), data.c_str());
}

void Window::setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::wstring& data) {
    xcb_change_property(connection()->handle(), mode, handle(), propertyAtom, typeAtom, 16, data.length() * 2, data.c_str());
}
 
void Window::setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::vector<uint8_t>& data) {
    xcb_change_property(connection()->handle(), mode, handle(), propertyAtom, typeAtom, 8, data.size(), &data.front());
}
 
void Window::setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::vector<uint16_t>& data) {
    xcb_change_property(connection()->handle(), mode, handle(), propertyAtom, typeAtom, 16, data.size() * 2, &data.front());
}
 
void Window::setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::vector<uint32_t>& data) {
    xcb_change_property(connection()->handle(), mode, handle(), propertyAtom, typeAtom, 32, data.size() * 4, &data.front());
}
 
void Window::setWindowName(const std::string& name) {
    setProperty(ReplaceProperty, WM_NAME, STRING, name);
}

void Window::setWindowName(const std::wstring& name) {
    setProperty(ReplaceProperty, WM_NAME, STRING, name);
}

void Window::setIconName(const std::string& name) {
    setProperty(ReplaceProperty, WM_ICON_NAME, STRING, name);
}

void Window::setIconName(const std::wstring& name) {
    setProperty(ReplaceProperty, WM_ICON_NAME, STRING, name);
}

void Window::move(uint16_t x, uint16_t y) {
    uint32_t val[] = { x, y };
    xcb_configure_window(connection()->handle(), handle(), XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, val);
}

void Window::resize(uint16_t w, uint16_t h) {
    uint32_t val[] = { w, h };
    xcb_configure_window(connection()->handle(), handle(), XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, val);
}

void Window::setGeometry(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
    uint32_t val[] = { x, y, w, h };
    xcb_configure_window(connection()->handle(), handle(), XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, val);
}

void Window::setBorderWidth(uint16_t w) {
    uint32_t val = w;
    xcb_configure_window(connection()->handle(), handle(), XCB_CONFIG_WINDOW_BORDER_WIDTH, &val);
}

void Window::raise() {
    uint32_t val = XCB_STACK_MODE_ABOVE;
    xcb_configure_window(connection()->handle(), handle(), XCB_CONFIG_WINDOW_STACK_MODE, &val);
}

void Window::lower() {
    uint32_t val = XCB_STACK_MODE_BELOW;
    xcb_configure_window(connection()->handle(), handle(), XCB_CONFIG_WINDOW_STACK_MODE, &val);
}

Window::Tree* Window::requestWindowTree() {
    return new Window::Tree(this);
}

struct Window::Tree::Private : public XOOB_COOKIE_TYPE(xcb_query_tree) {
    Window* window;
};

Window::Tree::Tree(Window* window) : d(new Window::Tree::Private) {
    d->connection = window->connection();
    d->window = window;
    d->cookie = xcb_query_tree(window->connection()->handle(), window->handle());
}

Window::Tree::~Tree() {
    delete d;
}

XOOB_COOKIE_IMPL(Window::Tree)

Window* Window::Tree::window() {
    return d->window;
}

Window* Window::Tree::root() {
    if(!waitForResponse()) return 0;
    return d->connection->registeredWindow(d->reply->root);
}

Window* Window::Tree::parent() {
    if(!waitForResponse()) return 0;
    return d->connection->registeredWindow(d->reply->parent);
}

uint16_t Window::Tree::childCount() {
    XOOB_COOKIE_RETURN(children_len);
}

XOOB_COOKIE_DEFAULT(Window::MappedPoint, xcb_translate_coordinates)

Window::MappedPoint::MappedPoint(Window* window, Window* other, int16_t x, int16_t y) : d(new Window::MappedPoint::Private) {
    d->connection = window->connection();
    d->cookie = xcb_translate_coordinates(d->connection->handle(), window->handle(), other->handle(), x, y);
}

Window::MappedPoint::~MappedPoint() {
    delete d;
}

int16_t Window::MappedPoint::x() {
    XOOB_COOKIE_RETURN(dst_x);
}

int16_t Window::MappedPoint::y() {
    XOOB_COOKIE_RETURN(dst_y);
}

XOOB_COOKIE_DEFAULT(Window::Attributes, xcb_get_window_attributes)

Window::Attributes::Attributes(Window* window) : d(new Window::Attributes::Private) {
    d->connection = window->connection();
    d->cookie = xcb_get_window_attributes(d->connection->handle(), window->handle());
}

Window::Attributes::~Attributes() {
    delete d;
}

uint8_t Window::Attributes::backingStore() {
    XOOB_COOKIE_RETURN(backing_store);
}

uint32_t Window::Attributes::visualID() {
    XOOB_COOKIE_RETURN(visual);
}

uint16_t Window::Attributes::windowClass() {
    XOOB_COOKIE_RETURN(_class);
}

uint8_t Window::Attributes::bitGravity() {
    XOOB_COOKIE_RETURN(bit_gravity);
}

uint8_t Window::Attributes::windowGravity() {
    XOOB_COOKIE_RETURN(win_gravity);
}

uint32_t Window::Attributes::backingPlanes() {
    XOOB_COOKIE_RETURN(backing_planes);
}

uint32_t Window::Attributes::backingPixel() {
    XOOB_COOKIE_RETURN(backing_pixel);
}

uint8_t Window::Attributes::saveUnder() {
    XOOB_COOKIE_RETURN(save_under);
}

uint8_t Window::Attributes::mapIsInstalled() {
    XOOB_COOKIE_RETURN(map_is_installed);
}

uint8_t Window::Attributes::mapState() {
    XOOB_COOKIE_RETURN(map_state);
}

uint8_t Window::Attributes::overrideRedirect() {
    XOOB_COOKIE_RETURN(override_redirect);
}

Colormap* Window::Attributes::colormap() {
    if(!waitForResponse()) return 0;
    return new xoob::Colormap(d->reply->colormap, d->connection);
}

uint32_t Window::Attributes::allEventMasks() {
    XOOB_COOKIE_RETURN(all_event_masks);
}

uint32_t Window::Attributes::eventMask() {
    XOOB_COOKIE_RETURN(your_event_mask);
}

uint16_t Window::Attributes::doNotPropagateMask() {
    XOOB_COOKIE_RETURN(do_not_propagate_mask);
}

}
