#ifndef XOOB_WINDOW_H
#define XOOB_WINDOW_H

#include "drawable.h"
#include "event.h"
#include <stdint.h>
#include <string>
#include <vector>

namespace xoob {

class Connection;
class Screen;
class Colormap;

class Window : public Drawable {
private:
    friend class Screen;
    class Private;
    Private* d;

    Window(uint32_t handle, Connection* connection);

public:
    class Tree;
    class MappedPoint;
    class Attributes;

    enum WindowClass {
        CopyFromParent,
        InputOutput,
        InputOnly
    };

    enum WindowOption {
        BackPixmap        = 1 << 0,
        BackPixel         = 1 << 1,
        BorderPixmap      = 1 << 2,
        BorderPixel       = 1 << 3,
        BitGravity        = 1 << 4,
        WindowGravity     = 1 << 5,
        BackingStore      = 1 << 6,
        BackingPlanes     = 1 << 7,
        BackingPixel      = 1 << 8,
        OverrideRedirect  = 1 << 9,
        SaveUnder         = 1 << 10,
        EventMask         = 1 << 11,
        DontPropagage     = 1 << 12,
        Colormap          = 1 << 13,
        Cursor            = 1 << 14
    };

    enum PropertyMode {
        ReplaceProperty,
        PrependProperty,
        AppendProperty
    };

    Window(Window* parent = 0, Connection* connection = 0);
    Window(WindowClass windowClass, uint8_t depth, Window* parent = 0, Connection* connection = 0);
    virtual ~Window();

    void show();
    void hide();

    void setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::string& data); 
    void setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::wstring& data); 
    void setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::vector<uint8_t>& data); 
    void setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::vector<uint16_t>& data); 
    void setProperty(PropertyMode mode, uint32_t propertyAtom, uint32_t typeAtom, const std::vector<uint32_t>& data); 

    void setWindowName(const std::string& name); 
    void setWindowName(const std::wstring& name); 
    void setIconName(const std::string& name); 
    void setIconName(const std::wstring& name); 

    void move(uint16_t x, uint16_t y);
    void resize(uint16_t w, uint16_t h);
    void setGeometry(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
    void setBorderWidth(uint16_t w); 
    void raise();
    void lower();

    Tree* requestWindowTree(); 
    MappedPoint* mapToWindow(Window* other, int16_t x, int16_t y);
    Attributes* requestAttributes();
};

class Window::Tree {
private:
    friend class Window;
    class Private;
    Private* d;

    Tree(Window* window);

public:
    ~Tree();

    bool waitForResponse();
    bool error();

    Window* window();
    Window* root();
    Window* parent();
    uint16_t childCount();
};

class Window::MappedPoint {
private:
    friend class Window;
    class Private; 
    Private* d;                 

    MappedPoint(Window* window, Window* other, int16_t x, int16_t y);

public:
    ~MappedPoint();

    bool waitForResponse(); 
    bool error();

    int16_t x();
    int16_t y();
};

class Window::Attributes {
private:
    friend class Window;
    class Private;
    Private* d;

    Attributes(Window* window);

public:
    ~Attributes();

    bool waitForResponse();
    bool error();

    uint8_t backingStore();
    uint32_t visualID();
    uint16_t windowClass();
    uint8_t bitGravity();
    uint8_t windowGravity();
    uint32_t backingPlanes();
    uint32_t backingPixel();
    uint8_t saveUnder();
    uint8_t mapIsInstalled();
    uint8_t mapState();
    uint8_t overrideRedirect();
    xoob::Colormap* colormap();
    uint32_t allEventMasks();
    uint32_t eventMask();
    uint16_t doNotPropagateMask();
};

};

#endif
