TEMPLATE = lib
TARGET = xoob
CONFIG += debug
CONFIG -= qt
LIBS += $$system(pkg-config --libs xcb-atom)
CXXFLAGS += $$system(pkg-config --cflags xcb-atom)

HEADERS += connection.h event.h screen.h drawable.h window.h graphicscontext.h geometry.h font.h colormap.h pixmap.h
SOURCES += connection.cpp event.cpp screen.cpp drawable.cpp window.cpp graphicscontext.cpp geometry.cpp font.cpp colormap.cpp pixmap.cpp
